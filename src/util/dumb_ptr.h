#ifndef UTIL_DUMB_PTR_H
#define UTIL_DUMB_PTR_H

#include <memory>

namespace util {

template <typename T>
class dumb_ptr {
public:
    explicit dumb_ptr(const std::unique_ptr<T> &unique):
        ptr{unique.get()}
    {}

    void operator=(const std::unique_ptr<T> &unique)
    {
        ptr = unique.get();
    }

    T *operator*()
    {
        return ptr;
    }

    T *operator->()
    {
        return *(*this);
    }

    operator bool()
    {
        return ptr;
    }

private:
    T* ptr;
};

template <typename T, typename ...Args>
dumb_ptr<T> make_dumb(Args &&...args)
{
    return dumb_ptr<T>{std::forward<Args>(args)...};
}

};

#endif
