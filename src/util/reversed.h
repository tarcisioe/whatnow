#ifndef UTIL_REVERSED_H
#define UTIL_REVERSED_H

#include <iterator>


namespace util {

template <typename Container>
class ReverseAdapter {
public:
    ReverseAdapter(Container &c):
        c(c)
    {}

    auto begin() {
        return std::make_reverse_iterator(std::end(c));
    }

    auto end() {
        return std::make_reverse_iterator(std::begin(c));
    }

private:
    Container &c;
};

template <typename Container>
auto reversed(Container &c) {
    return ReverseAdapter<Container>{c};
}

}

#endif
