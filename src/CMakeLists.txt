cmake_minimum_required(VERSION 3.0)
project(whatnow)

set(CMAKE_MODULE_PATH
    "${CMAKE_SOURCE_DIR}/cmake"
    "${CMAKE_SOURCE_DIR}/cmake-utils"
)

include(utils)

enable_testing()

add_subdirectory(util)
add_subdirectory(sort)
add_subdirectory(app)

foreach(_target ${TARGETS})
    target_compile_features(${_target} PRIVATE cxx_std_17)
endforeach()


UTILS_GENERATE_CLANG_COMPLETE()
