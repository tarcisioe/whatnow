#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

#include "merge_sort.h"
#include "reversed.h"

bool compare(const std::string &a, const std::string &b)
{
    std::cout << "1 - " << a << "\n";
    std::cout << "or" << "\n";
    std::cout << "2 - " << b << "\n";
    std::cout << "?" << "\n";

    auto choice = 0;
    while (!(std::cin >> choice) || !(choice == 1 || choice == 2)) {
        std::cerr << "Please choose 1 or 2\n";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    return choice == 2;
}

std::vector<std::string> lines_from_file(const std::string &filename)
{
    auto line = std::string{};
    std::ifstream infile{filename};
    auto lines = std::vector<std::string>{};

    while (std::getline(infile, line))
    {
        lines.push_back(move(line));
    }

    return lines;
}

int main(int argc, char **argv) {
    using util::reversed;

    if (argc < 2) {
        std::cerr << "Please provide a filename.\n";
        return -1;
    }

    auto lines = lines_from_file(argv[1]);

    sort::merge_sort(lines, compare);

    for (auto & x: reversed(lines)) {
        std::cout << x << "\n";
    }
}
