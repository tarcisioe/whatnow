#include <cassert>
#include <algorithm>
#include <vector>

#include "merge_sort.h"


int main()
{
    using sort::merge_sort;

    auto original = std::vector<std::string>{"john", "eric", "terry", "terry", "graham", "michael"};
    auto sorted = original;
    std::sort(begin(sorted), end(sorted));
    auto reversed = original;
    std::sort(begin(reversed), end(reversed), [](const std::string &a, const std::string &b){ return a > b; });

    {
        auto x = original;
        merge_sort(x);
        assert(std::equal(begin(x), end(x), begin(sorted)));
    }

    {
        auto x = original;
        merge_sort(x, [](const std::string &a, const std::string &b){ return a > b; });
        assert(std::equal(begin(x), end(x), begin(reversed)));
    }

}
