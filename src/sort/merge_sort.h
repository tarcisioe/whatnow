#include <iterator>
#include <vector>


namespace sort {
    template <typename It, typename Comp>
    void merge(It start, It middle, It end, Comp comp) {
        using T = typename std::remove_reference_t<decltype(*start)>;
        auto aux = std::vector<T>{};

        auto i = start;
        auto j = middle;

        while (i != middle or j != end) {
            if (j == end or (i != middle && comp(*i, *j))) {
                aux.emplace_back(std::move(*i));
                ++i;
            } else {
                aux.emplace_back(std::move(*j));
                ++j;
            }
        }

        std::move(std::begin(aux), std::end(aux), start);
    }


    template <typename It, typename Comp>
    void merge_sort(It start, It end, Comp comp) {
        auto distance = std::distance(start, end);

        if (distance < 2) { return; }

        auto middle = start;
        auto midpoint = distance/2;
        std::advance(middle, midpoint);

        merge_sort(start, middle, comp);
        merge_sort(middle, end, comp);
        merge(start, middle, end, comp);
    }


    template <typename It>
    void merge_sort(It start, It end) {
        merge_sort(start, end, std::less<decltype(*start)>{});
    }


    template <typename Sequence>
    void merge_sort(Sequence &s) {
        merge_sort(begin(s), end(s));
    }


    template <typename Sequence, typename Comp>
    void merge_sort(Sequence &s, Comp comp) {
        merge_sort(begin(s), end(s), comp);
    }
}
